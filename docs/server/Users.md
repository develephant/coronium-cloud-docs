# user.addUser

```lua
coronium.user.addUser( registrationData )
```

---

Adds a new user.

__Parameters__

Name|Details
----|-------
registrationData|A table of data to be stored for the User record. __An email and password key are required for the User log in__. (Table, required)

__Returns__



__Examples__

```lua
local userData = {
  email = "user@email.com", --REQUIRED
  password = "2468", --REQUIRED
  fav_color = "Blue",
  age = 24,
  dogs = { "danny", "fido", "suzy" }
}

local answer = coronium.user.addUser( userData )

if not answer.error then
  coronium.log( "Added user" )
end
```

---

# user.getUser

```lua
coronium.user.getUser( userId, filter_fields )
```

---

Gets user data based on userId.

__Parameters__

Name|Details
----|-------
userId|The users objectId. (String, required)
filter_fields|Filter fields. See getUsers(). (Table, optional)

__Returns__

__Examples__

```lua
local answer = coronium.user.getUser( "userd-id" )
if not answer.error then
  coronium.log( answer.result.email )
end
```

---

# user.getUsers

```lua
coronium.user.getUsers( query_table, filter_fields, limit, sort_field, sort_order )
```

---

Gets user records based on query. Will return all records if query is empty.

__Parameters__

Name|Details
----|-------
query_table|The query table. If not passed, all records will be returned. (Table, optional)
filter_fields|Filter fields. (Table, optional)
limit|Record limit to return. If not passed, all records will be returned. (Number, optional)
sort_field|The key to sort on. (String, optional)
sort_order|The order of the sort. 'ASC' or 'DESC' (String, optional)

!!! info "Results"
    The __answer.result__ will be a table array with the records. You can loop over them with __ipairs__.

__Returns__

__Examples__

```lua
-- All users
local answer = coronium.user.getUsers()

--Query based
local answer = coronium.user.getUsers( { coffee = "Black" } )

--Get 20 users max
local answer = coronium.user.getUsers( {}, {}, 20 )

--Limit returned fields.  This returns only email field from records.
local answer = coronium.user.getUsers( {}, { email = 1 } )

--Limit returned fields.  This returns all fields except age from records.
local answer = coronium.user.getUsers( {}, { age = 0 } )

--Full users hits usage with field filter, limit, and sort.
local answer = coronium.user.getUsers( { platform = "ios",
hits = { ["$gt"] = 100 } }, { hits = 1, email = 1 }, 10, 'hits', 'DESC' )

--Looping over the result records
local user_records = answer.result
for _, user in ipairs( user_records ) do
  print( user.email, user.hits )
end
```

---

# modifyUser

```lua
coronium.user.modifyUser( userId, modificationData )
```

---

Modify a user record.

__Parameters__

Name|Details
----|-------
userId|The user id to modify. (String, required)
modificationData|A table of data to be updated for the User record. (Table, required)

!!! warning "Heads Up!"
    When updating a user password, __if it is a plain string__, you must mime b64 encode it. You can use the __coronium.utils.encode_b64("str_password")__ method if needed. ___Be careful not to double encode___. See example below.

__Returns__

__Examples__

```lua
local modifyData = {
  food = "Apples",
  age = 21,
  coins = 123000
}

local answer = coronium.user.modifyUser( userId, modifyData )

if not answer.error then
  coronium.log( "User modified" )
end
```

_Updating Passwords_

```lua
-- When updating a password, if it is a plain string,
-- make sure to mime b64 encode it.
local modifyData = {
  password = coronium.utils.encode_b64( "string_password " )
}
```

---

# user.deleteUser

```lua
coronium.user.deleteUser( userId )
```

---

Removes a user.

__Parameters__

Name|Details
----|-------
userId|The users id to remove. (String, required)

__Returns__

__Examples__

```lua
local answer = coronium.user.deleteUser( "user-id" )
  if not answer.error then
    coronium.log( "user deleted" )
  end
```

---

# user.addDevice

```lua
coronium.user.addDevice( userId, deviceData )
```

---

Add a user device to a user record.

__Parameters__

Name|Details
----|-------
userId|The users id. (String, required)
deviceData|The device data. (Table, required)

__Returns__

__Examples__

```lua
local deviceData = {
  token = "device-token", --required
  platform = coronium.IOS -- coronium.IOS or coronium.ANDROID, required.
}

local answer = coronium.user.addDevice( "user-id", deviceData )

if not answer.error then
  coronium.log( "Device added" )
end
```

---

# user.removeDevice

```lua
coronium.user.removeDevice( userId, deviceData )
```

---

Removes a user device from a user record.

__Parameters__

Name|Details
----|-------
userId|The users id. (String, required)
deviceData|The device data. (Table, required)

__Returns__

__Examples__

```lua
local deviceData = {
  token = "device-token", --required
}

local answer = coronium.user.removeDevice( "user-id", deviceData )

if not answer.error then
  coronium.log( "Device cleared" )
end
```

---

# user.push

```lua
coronium.user.push( userId, message, badge, sound, custom )
```

---

Send a push to the specified user.

__Parameters__

Name|Details
----|-------
userId|The users id. (String, required)
message|The push message data. (String, required)
badge|The message badge count. (String, optional)
sound|A custom included alert sound. (String, optional)
custom|A table used for deep linking. (Table, optional)

__Returns__

__Examples__

!!! warning "Heads Up!"
    You need to be signed up for the FREE [PushBots.com](http://pushbots.com) service to enable push functionality. You must also include a device on the users record using __coronium.user.addDevice()__

```lua
local answer = coronium.user.push( "user-id", "This is a message",
"+1", "custom-snd", { username = "Chris" } )

if not answer.error then
  coronium.log( "Push sent" )
end
```

---

# user.clearSession

```lua
coronium.user.clearSession( userId )
```

---

Deletes a user session.

__Parameters__

Name|Details
----|-------
userId|The users id of the session. (String, required)

__Returns__

__Examples__

```lua
local answer = coronium.user.clearSession( "user-id" )
  if not answer.error then
    coronium.log( "session cleared" )
  end
```
