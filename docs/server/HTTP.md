# network.get

```lua
coronium.network.get( uri, uri_args, headers )
```

Makes a GET method call with uri_args to the uri provided. Optional headers can be sent.

__Parameters__

Name|Details
----|-------
uri|The uri to call the GET request on. (String, required)
uri_args|Additonal params to send. (String, optional)
headers|Additional headers. (Table, optional)

__Returns__

An __[Answer](/devel/Answer)__ table.

__Examples__

```lua
local args = { color = "red", limit = 20 }
local uri_args = table.concat( args, '&' )
local answer = coronium.network.get('http://some-endpoint', uri_args )
```

Arguments are appended to the 'GET' request uri, so the above call would resolve to:

```bash
http://some-endpoint?color=red&limit=20
```

# network.post

```lua
coronium.network.post( uri, bodyStr, headers )
```

---

Makes a POST method call with body string to the uri provided. Optional headers can be sent.

!!! note "Post Body"
    When 'posting', it is the developers task to make sure the post body is a string format compatible with a 'POST' request.

__Parameters__

Name|Details
----|-------
uri|The uri to call the POST request on. (String, required)
bodyStr|The contents to send. (String, required)
headers|Additional headers. (Table, optional)

__Returns__

An __[Answer](/devel/Answer)__ table.

__Examples__

When you 'POST', the body you send needs to be in a form-encodable format. You can change the "Content-Type" header to override the post type, which by default is:

```bash
"Content-Type": "application/x-www-form-urlencoded"
```

The simplest way to put together a proper POST request, is to build a keyed table that mimics an html "form".

```lua
local tmpForm = --String values must be url-encoded
{
  username = coronium.utils.escape("SuperUser McGee"),
  dogs = true
}
-- Convert this "form" to a post-able string.
local postStr = table.concat( tmpForm, '&' )
local answer = coronium.network.post('http://some-endpoint', postStr)
```

If you're looking to send JSON, see __[network.postJson](#networkpostjson)__.

# network.request

```lua
coronium.network.request( uri, body, method, headers )
```

---

Makes a specific method call with body to the uri provided. Optional headers can be sent. Supports the methods GET, POST, DELETE, PUT, HEAD, etc.

!!! note "Post Body"
    When 'posting', it is the developers task to make sure the post body is a string format compatible with a 'POST' request.

__Parameters__

Name|Details
----|-------
uri|The uri to call the request on. (String, required)
body|The content to send. (String, required)
method|The request method to use. (String, required)
headers|Additional headers. (Table, optional)

__Returns__

An __[Answer](/devel/Answer)__ table.

__Examples__

```lua
local params_body =
{
  id = coronium.utils.escape( "1234" )
}
params_body = table.concat(params_body, '&')

local spec_header =
{
  ["X-Super-ID"] = "some-super-identifier"
}

local answer = coronium.network.request(
  'http://some-endpoint',
  params_body,
  'DELETE',
  spec_header )
```

# network.getJson

```lua
coronium.network.getJson( uri, args_tbl, headers )
```

---

Makes a GET method call with uri_args to the uri provided. Optional headers can be sent. The default "Content-Type" header for a JSON call is 'application/json'.

__Parameters__

Name|Details
----|-------
uri|The uri to call the request on. (String, required)
args_str|Additonal parameters to send. (String, optional)
headers|Additional headers. (Table, optional)

__Returns__

An __[Answer](/devel/Answer)__ table.

__Examples__

```lua
local args = { count = 20, sort = true }
local args_str = table.concat( args, '&')
local answer = coronium.network.getJson('http://some-endpoint', args_str)
```

# network.postJson

```lua
coronium.network.postJson( uri, data_table, headers )
```

---

Makes a POST method call with data_table to the uri provided. Optional headers can be sent. Sends table data as JSON. The default "Content-Type" header for a JSON call is 'application/json'.

!!! warning "Table Body"
    When using __postJson__, make sure to pass in a keyed table as the 'body', ___not a string___.
    
The `data_table` will automatically be converted to JSON, and the "Content-Type" header will be set to 'application/json' before posting.

__Parameters__

Name|Details
----|-------
uri|The uri to call the request on. (String, required)
data_table|Data table to post. (Table, required)
headers|Additional headers. (Table, optional)

__Returns__

An __[Answer](/devel/Answer)__ table.

__Examples__

```lua
-- Default "Content-Type" header will be 'application/json'
local dataTbl = { username = "Jimmy", profile = { age = 27, cats = 12 } }
local answer = coronium.network.postJson('http://some-endpoint', dataTbl)
```

---

# Examples

__getJSON__

```lua
-- Collect incoming data
local in_data = coronium.input()

-- Get user location value from data
local location = in_data.location

-- Call weather endpoint with location, produces JSON content.
local endpoint = "http://api.openweathermap.org/data/2.5/weather?q=" .. location

-- Get the answer table from the network.getJson method.
-- Valid JSON "body" is converted to a LUA table in answer result.
local answer = coronium.network.getJson( endpoint )

-- Return the answer table to client
coronium.output( answer )
```

__Standard Post__

```lua
local uri = 'http://some-endpoint'
local body = "color=blue&id=12"
coronium.output( coronium.network.post( uri, body ) )
```

__Video Tutorial__

[Video Tutorial on YouTube](https://www.youtube.com/watch?v=g3_zlty1biA)
