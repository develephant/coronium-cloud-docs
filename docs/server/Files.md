# Storing Files

__[See the uploadFile client API method](../client/Files)__

# Accessing Files

When you successfully upload a file resource you will receive an file URI back from Coronium. Using the URI, you can access the file from your Coronium instance endpoint.

!!! note "Example file path return:"
    __your.coronium.instance/files/pngs/aaYte553A77rad-image.png__

# Video Tutorial

[Video Tutorial on YouTube](https://www.youtube.com/watch?v=Wxs9Pwvje_k)
