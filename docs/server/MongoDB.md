# mongo:createObject

```lua
coronium.mongo:createObject( collection, data_table )
```

---

Saves the data_table to MongoDB under the supplied collection.

!!! note "Object IDs"
    createObject method will also create an __objectId__ key with a unique object id that you can use in other methods.

__Parameters__

Name|Details
----|-------
collection|The collection name to add this object to. (String, required)
data_table|The data record to add to the collection. (Table, required)

__Returns__

__Examples__

```lua
local answer = coronium.mongo:createObject( "users",
{ username = "John" } )
if not answer.error then
  print( answer.result.objectId )
end
```

---

# mongo:getObject

```lua
coronium.mongo:getObject( collection, objectId )
```

---

Returns a data table from MongoDB based on the supplied objectId

__Parameters__

Name|Details
----|-------
collection|The collection name to access. (String, required)
objectId|The record object id in the collection. (String, required)

__Returns__

__Examples__

```lua
local data_table
local answer = coronium.mongo:getObject( "users", "abcd1234" )
if not answer.error then
  data_table = answer.result
end
```

---

# mongo:findObject

```lua
coronium.mongo:findObject( collection, query_table, show_keys )
```

---

Returns a data table from MongoDB based on the supplied query. Optional shown key flags

__Parameters__

Name|Details
----|-------
collection|The collection to perform the find with. (String, required)
query_table|The query table used to filter the results. (Table, required)
show_keys|Select specific keys you would like to retrieve. (Table, optional)

__Returns__

__Examples__

```lua
local answer = coronium.mongo:findObject( "users",
{ username = "Smith" }, { age = 0 } )
if not answer.error then
  print( answer.result.username )
end
```

---

# mongo:getObjects

```lua
coronium.mongo:getObjects( collection, query_tbl, limit, sortField, sortOrder )
```

---

Searches a collection based on the provided parameters. The 'query_tbl' is a table with keys you'd like to search against.

__Parameters__

Name|Details
----|-------
collection|The collection to get objects from. (String, required)
query_tbl|The query table to filter objects with. (Table, required)
limit|The amount of results requested. (Number, optional)
sortField|The object field to sort on. (String, optional)
sortOrder|The sort order for the result set. Can be __ASC__ or __DESC__. (Constant, optional)

__Returns__

__Examples__

```lua
local data_table
local answer = coronium.mongo:getObjects( "users",
{ points = { ["$gt"] = 1000 } )
if not answer.error then
  data_table = answer.result
end
```

_Query "dogs" with age and color criteria, return up to 100 records (default)_

```lua
local answer = coronium.mongo:getObjects( "dogs", { age = 12, color = "Black" } )
```

_Get any "dogs" record, up to 200 results._

```lua
local answer = coronium.mongo:getObjects( "dogs", {}, 200 )
```

_Get up to 50 records and sort by "age" descending._

```lua
local answer = coronium.mongo:getObjects( "dogs", {}, 50, "age", "DESC" )
```

!!! note "Search Tips"
    You can use special operators for searching: __$lt__ - less than | __$lte__ - less or equal | __$gt__ - greater than | __$gte__ - greater or equal | __$ne__ - not equal, useful for testing empties.

```lua
-- example 1: users named 'Lori'
local answer = coronium.mongo:getObjects( "users",
{ username = "Lori" } )
-- example 2: all scores less than 1000
local answer = coronium.mongo:getObjects( "scores",
{ score = { ["$lt"] = 1000 } } )
-- example 3: user is admin and score is greater or equal to 2500
local answer = coronium.mongo:getObjects( "users",
{ admin = true, score = { ["$gte"] = 2500 } } )
-- get result
if not answer.error then
  print( #answer.result )
end
```

---

# mongo:updateObject

```lua
coronium.mongo:updateObject( collection, objectId, data_table )
```

---

Updates a stored object with the supplied objectId and updated data table.

__Parameters__

Name|Details
----|-------
collection|The collection name to reference. (String, required)
objectId|The objectId of the object to update. (Table, required)
data_table|The keys to update on the record. (Table, required)

__Returns__

__Examples__

```lua
local answer = coronium.mongo:updateObject( "users", "abcd1234",
{ admin = true } )
if not answer.error then
  print( "updated" )
end
```

---

# mongo:updateObjects

```lua
coronium.mongo:updateObjects( collection, query, update_table )
```

---

Updates stored objects with the supplied query and updated data table.

__Parameters__

Name|Details
----|-------
collection|The collection name to reference. (String, required)
query|A query table to filter update objects. (Table, required)
update_table|The keys to inc/dec on including amounts. (Table, required)

__Returns__

__Examples__

```lua
--Set all users whose boots are leather and hunger is satiated to a ready state.
local answer = coronium.mongo:updateObjects( "users",
{ boots = "Leather", hunger = 100 }, { ready = true } )
if not answer.error then
  print( "updated user objects" )
end
```

---

# mongo:incObjectKeys

```lua
coronium.mongo:incObjectKeys( collection, objectId, inc_data_table )
```

---

Updates the keys numerical count by N for Object belonging to objectId.

__Parameters__

Name|Details
----|-------
collection|The collection name to reference. (String, required)
objectId|The objectId of the object to inc/dec. (String, required)
inc_data_table|The keys to inc/dec on including amounts. (Table, required)

__Returns__

__Examples__

```lua
-- Increments the score 12000 points, must be number type.
local answer = coronium.mongo:incObjectKeys( "users", "abcd1234",
{ score : 12000 } )
if not answer.error then
  print( "Score adjusted." )
end

-- Removes 25 Karma points, Adds 20 warning, must be number type.
local answer = coronium.mongo:incObjectKeys( "users", "abcd1234",
{ karma : -25, warning: 20 } )
if not answer.error then
  print( "Karma and warning adjusted." )
end
```

---

# mongo:removeObjectKeys

```lua
coronium.mongo:removeObjectKeys( collection, objectId, keys_to_remove_table )
```

---

Removes objectId keys and associated data from the provided table array of keys.

__Parameters__

Name|Details
----|-------
collection|The collection name to reference. (String, required)
objectId|The objectId of the object to remove keys from. (String, required)
keys_to_remove_table|A table array of the keys to remove from the object. (Table, required)

__Returns__

__Examples__

```lua
local data_table
--Removes the age and height key for this users data object
local answer = coronium.mongo:removeObjectKeys( "users", "abcd1234",
{ "age", "height" } )
if not answer.error then
  data_table = answer.result
end
```

---

# mongo:deleteObject

```lua
coronium.mongo:deleteObject( collection, objectId )
```

---

Deletes an object from MongoDB based on the supplied objectId.

__Parameters__

Name|Details
----|-------
collection|The collection name to reference. (String, required)
objectId|The objectId of the object to delete. (String, required)

__Returns__

__Examples__

```lua
local answer = coronium.mongo:deleteObject( "users", "abcd1234" )
if not answer.error then
  print( "deleted" )
end
```

---

# mongo:deleteObjects

```lua
coronium.mongo:deleteObjects( collection, query )
```

---

Deletes objects from MongoDB based on the supplied query.

__Parameters__

Name|Details
----|-------
collection|The collection name to reference. (String, required)
query|The query table to filter results. (Table, required)

__Returns__

__Examples__

```lua
local answer = coronium.mongo:deleteObjects( "users", { canceled : true } )
if not answer.error then
  print( "deleted canceled objects" )
end
```

---

# mongo:getObjectCount

```lua
coronium.mongo:getObjectCount( collection, query_table )
```

---

Counts the records in collection based on the provided query_table.

__Parameters__

Name|Details
----|-------
collection|The collection name to count on. (String, required)
query_table|The query table to filter results. (Table, optional)

__Returns__

__Examples__


# Video Tutorial

[Video Tutorial on YouTube](https://www.youtube.com/watch?v=cIm4EArOh2I)
