# crypto.hash

```lua
coronium.crypto.hash( string )
```
---

Create a MD5 hash.

__Parameters__

Name|Details
----|-------
string|A string to MD5. (String, required)

__Returns__

A MD5 hash.

__Examples__

```lua
local hashmd5 = coronium.crypto.hash( str )
```

---

# crypto.digest

```lua
coronium.crypto.digest( password )
```
---

Creates an encrytped password.

__Parameters__

Name|Details
----|-------
string|A password to encrypt. (String, required)

__Returns__

Encrypted password

__Examples__

```lua
local enc_password = coronium.crypto.digest( str_password )
```

---

# crypto.verify

```lua
coronium.crypto.verify( password, digest )
```
---

Verify a string password against a digest.

__Parameters__

Name|Details
----|-------
password|A string password. (String, required)
digest|Encrypted password. (String, required)

__Returns__

Boolean. True if verified.

__Examples__

```lua
local verified = coronium.crypto.verify( password, digest )
```

---

# crypto.hmac_sha1

```lua
coronium.crypto.hmac_sha1( string )
```
---

Create a sha hash.

__Parameters__

Name|Details
----|-------
string|A string to hmac_sha1. (String, required)

__Returns__

A sha1 hash.

__Examples__

```lua
local strsha = coronium.crypto.hmac_sha1( str )
```

---
