# php.run

```lua
coronium.php.run( scriptName, args_or_post_table ,http_method )
```

---

Executes a local PHP page and returns the results.

__Parameters__

Name|Details
----|-------
scriptName|The name of the PHP file, without the extension. For example, to call a file named hello.php, you would just pass “hello”.
args_or_post_table|A data table that will be used as data for POST, GET, PUT, or DELETE HTTP methods.
http_method|You can pass a specific HTTP method if needed. Defaults to “POST”

!!! warning "Heads Up!"
    The PHP file must reside in the Coronium __PHP__ dierctory.

__Returns__

Name|Details
----|-------
.result|Holds the PHP result as a Lua table.
.error|The error, if any. If there is no error it will be nil.
.errorCode|The error code, if any. Holds HTTP error status code.

__Examples__

_In /php/greeting.php_

```php
<?php
  $in_data = json_decode(file_get_contents('php://input'));

  $greeting = 'Hello ' . $in_data->{"username"} . '!';

  $response = array("greeting" => $greeting);

  print(json_encode($response));
?>
```

_In /lua/hello.lua_

```lua
local in_data = coronium.input()

 local answer = coronium.php.run( 'greeting', in_data )

 coronium.output( answer )
```

# Video Tutorial

[Video Tutorial on YouTube](http://www.youtube.com/watch?v=GSNQHp2UmZk)
