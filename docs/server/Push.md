!!! warning "PushBots Account Required"
    You need to be signed up for the FREE [PushBots.com](http://pushbots.com) service to enable push functionality.

---

# push.registerDevice

```lua
coronium.push.registerDevice( deviceToken, platform, tagsTbl )
```

---

Registers a device with PushBots.com.

__Parameters__

Name|Details
----|-------
deviceToken|The device token. (String, required)
platform|The device platform. Can be __coronium.IOS__ or __coronium.ANDROID__ (Constant, required)
tagsTbl|Table array of tag names. (Table array, optional)

__Returns__


__Examples__

```lua
local answer = coronium.push.registerDevice( "device-token", coronium.ANDROID )

if not answer.error then
  coronium.log( "Device registered" )
end
```

---

# push.unregisterDevice

```lua
coronium.push.unregisterDevice( deviceToken, platform )
```

---

Unregisters a device from PushBots.com.

__Parameters__

Name|Details
----|-------
deviceToken|The device token. (String, required)
platform|The device platform. Can be __coronium.IOS__ or __coronium.ANDROID__ (Constant, required)

__Returns__

__Examples__

```lua
local answer = coronium.push.unregisterDevice( "device-token", coronium.ANDROID )

if not answer.error then
  coronium.log( "Device unregistered" )
end
```

---

# push.to

coronium.push.to( deviceToken, platform, message, badge, sound, custom )

---

Sends a push message to a device user.

__Parameters__

Name|Details
----|-------
deviceToken|The device token. (String, required)
platform|The device platform. Can be __coronium.IOS__ or __coronium.ANDROID__ (Constant, required)
message|The push message. (String, required)
badge|Badge count. (String, optional)
sound|An included notification sound. (String, optional)
custom|Custom table for deep linking. (Table, optional)

__Returns__

__Examples__

```lua
local answer = coronium.push.to( "device-token", coronium.IOS, "Hello there!" )

if not answer.error then
  coronium.log( "Pushed!" )
end
```

---

# push.broadcast

```lua
coronium.push.broadcast( platform, message, tags, badge, sound, custom )
```

---

Sends a push message to all devices.

__Parameters__

Name|Details
----|-------
platform|The device platform. Can be __coronium.IOS__ and/or __coronium.ANDROID__ (Table array, Constants, required)
message|The push message. (String, required)
tags|Tags to send message to. (Table array, optional)
badge|Badge count. (String, optional)
sound|An included notification sound. (String, optional)
custom|Custom table for deep linking. (Table, optional)

__Returns__

__Examples__

```lua
local answer = coronium.push.broadcast(
  { coronium.IOS, coronium.ANDROID }, --platforms
  "This is a push message",           --message
  { "funny", "west coast" },          --tags (optional)
  "4",                                --badge count (optional)
  "custom-alert.wav",                 --custom sound (optional)
  { section = "Home" }                --custom table (optional)
)

if not answer.error then
  coronium.log( "Pushed!" )
end
```

---

# push.tagDevice

```lua
coronium.push.tagDevice( deviceToken, platform, tag )
```

---

Adds a tag to a PushBots device.

__Parameters__

Name|Details
----|-------
deviceToken|The device token. (String, required)
platform|The device platform. Can be __coronium.IOS__ or __coronium.ANDROID__ (Constant, required)
tag|Tag name. (String, required)

__Returns__

__Examples__

```lua
local answer = coronium.push.tagDevice( "device-token", coronium.IOS, "SomeTag" )

if not answer.error then
  coronium.log( "Device tagged" )
end
```

---

# push.untagDevice

```lua
coronium.push.untagDevice( deviceToken, platform, tag )
```

---

Removes a tag from a PushBots device.

__Parameters__

Name|Details
----|-------
deviceToken|The device token. (String, required)
platform|The device platform. Can be __coronium.IOS__ or __coronium.ANDROID__ (Constant, required)
tag|Tag name. (String, required)

__Returns__

__Examples__

```lua
local answer = coronium.push.untagDevice( "device-token", coronium.IOS, "SomeTag" )

if not answer.error then
  coronium.log( "Device untagged" )
end
```

---

# push.setBadgeCount

```lua
coronium.push.setBadgeCount( deviceToken, platform, count )
```

---

Adjusts the badge count for the device.

__Parameters__

Name|Details
----|-------
deviceToken|The device token. (String, required)
platform|The device platform. Can be __coronium.IOS__ or __coronium.ANDROID__ (Constant, required)
count|The new badge count. (String, required)

__Returns__

__Examples__

```lua
local answer = coronium.push.setBadgeCount( "device-token", coronium.IOS, "2" )

if not answer.error then
  coronium.log( "Badge updated" )
end
```
