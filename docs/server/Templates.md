# template.render

```lua
coronium.template.render( template_file, data_table )
```
---

Renders result as parsed template_file with included data_table. Text is in answer.result variable.

!!! warning "Heads Up!"
    Template file must reside in __/tpl__ folder on the server with an extension of ”.tpl”. When passing the __template_file__ variable, do not include the extension.

__Parameters__

Name|Details
----|-------
template_file|The name of the template file to load. (String, required)
data_table|A data table of tokens to be merged into the template. (Table, required)

__Returns__

A rendered string based on the contents file and data table.

__Examples__

```lua
--== Loads tpl/greeting.tpl as template source
local answer = coronium.template.render('greeting',{user="Chris"})
local final_str = answer.result
```

_See Usage below for more examples._

---

# template.renderString

```lua
coronium.template.renderString( template_string, data_table )
```

---

Renders result as parsed template_string with included data_table. Text is in __answer.result__ variable.

__Parameters__

Name|Details
----|-------
template_string|A string with template tokens. (String, required)
data_table|The data table with tokens for the template. (Table, required)

__Returns__

The string with tokens replaced.

__Examples__

```lua
local answer = coronium.template.renderString("Hi <%= user %>", {user="Chris"})
local final_str = answer.result
```

# Template Tags

Tag|Details
---|-------
<%= key_name %>|Prints the “key_name” value with an added line break, escaped.
<%= key_name -%>|Prints the “key_name” value without an added return line, escaped.
<%- key_name %>|Prints the “key_name” value without escaped chars (good for HTML values).
<% lua code to run %>|Runs the Lua code contained in the tag inline.

# Usage

_Template file (tpl/score_email.tpl)_

```html
<body>
  <div id="main">
    <strong>Hello, <%= username -%></strong>
    <p>Your last high score was <%= lastscore -%>!</p>
  </div>
</body>
```

_Cloud Lua (lua/score_email.lua)_

```lua
-- Populate the template variables
local vars = {
  username = "Robert",
  lastscore = 4500
}

-- Parse the template with the variables
local answer = coronium.template.render( 'score_email', vars )
local html_text = answer.result

-- EXAMPLE: Use the result for an email.
--local mail_options = {...
coronium.network.email( plain_text, html_text, mail_options )

-- Send parse result to client
coronium.output( answer )
```

# Looping Data

_Cloud Lua (lua/scores.lua)_

```lua
local scores = {
 { username = "Charles", score = 123000 },
 { username = "Jen", score = 145000 }
}

local vars = { scores = scores }

local answer = coronium.template.parse( 'scores', vars )
```

_Template file (tpl/scores.tpl)_

```html
<table>
  <% for i, item in ipairs(scores) do %>
    <tr><td><%= item.username -%></td><td><%= item.score -%></td></tr>
  <% end %>
</table>
```

# Video Tutorial

[Video Tutorial on YouTube](https://www.youtube.com/watch?v=WGBTlRi2TYs)
