# network.email

```lua
coronium.network.email( html_body, options_table )
```

---

Send an email with the provided html. You can also include attachments in the __options_table__ (see Attachments below).

__Examples__

__Returns__

```lua
-- Content to be displayed.
local html_body =
[[
<body>
  <h2>Hello</h2>
  <p>This is some <strong><i>fancy</i></strong> text.<p>
</body>
]]

-- Email message subject, sender, and receiver
local options = {
  subject = "We really liked your score.",
  to = { name = "Game Player", email = "game@email.com" },
  from = { name = "Game Inc", email = "noreply@game.com" },
}

-- Capture answer table from the network.email() method.
local answer = coronium.network.email( html_body, options )

-- Send answer to client.
coronium.output( answer )
```

# Attachments

To attach files to the email, pass a table array in the __attachments__ key of the options table.

!!! warning "Heads Up!"
    You must derive the attachment file path by using __coronium.io.getPath( basePath, fileName )__.

```lua
-- Email message subject, sender, receiver, and attachments.
local options = {
  subject = "We really liked your score.  Have some stuff.",
  to = { name = "Game Player", email = "game@email.com" },
  from = { name = "Game Inc", email = "noreply@game.com" },
  attachments = { coronium.io.getPath( "files", "my-file-to-attach.zip" ) }
}
```

The `basePath` "__files__" is the upload directory of your Coronium instance. It's simple to add multiple attachments as well:

```lua
--Using files from the uploaded "files" directory of your instance.
local attachment_1 = coronium.io.getPath( "files", "some-file-to-add.mov" )
local attachment_2 = coronium.io.getPath( "files", "my-friends-picture.jpg" )

local options = {
  subject = "We really liked your score.  Have some stuff.",
  to = { name = "Game Player", email = "game@email.com" },
  from = { name = "Game Inc", email = "noreply@game.com" },
  attachments = { attachment_1, attachment_2 }
}
```

# Templates

You can adjust the various email and web templates for account confirmation and password reset in the __/tpl__ folder on the Coronium Server.

!!! danger "Heads Up!"
    Be careful not to remove the template tags, which start and end with __<% %>__.

# Video Tutorial

[Video Tutorial on YouTube](https://www.youtube.com/watch?v=u8Pf-Ky1-jU)
