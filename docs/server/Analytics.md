# Capturing Events

__[See the Analytics client API methods.](../client/Analytics.md)__

## Viewing Analytics

You can view event breakdowns in your Coronium Admin in the Analytics section.
