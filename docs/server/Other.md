!!! warning "Heads Up!"
    __The following methods DO NOT return an "Answer" table.__

---

# log

```lua
coronium.log( string, string2, stringN )
```
---

Print out debugging messages to the __cloud.log__ file.

__Examples__

```lua
coronium.log( "something happened" )
```

```lua
coronium.log( "something happened", t.user, "again" )
```

---

# utils.mysqlString

```lua
coronium.utils.mysqlString( string )
```

---

Makes a string safe for MySQL.

__Parameters__

Name|Details
----|-------
string|The string to make safe. (String, required)

__Returns__

MySQL safe string.

__Examples__

```lua
local sql_safe_str = coronium.utils.mysqlString( str )
```

---

# utils.encode_b64

```lua
coronium.utils.encode_b64( string )
```

---

Encode a Base64 string.

__Parameters__

Name|Details
----|-------
string|A string to Base64 encode. (String, required)

__Returns__

A Base64 encoded string.

__Examples__

```lua
local encodedb64 = coronium.utils.encode_b64( str )
```

---

# utils.decode_b64

```lua
coronium.utils.decode_b64( base64_str )
```

---

Decode a Base64 string.

__Parameters__

Name|Details
----|-------
string|A Base64 string to decode. (String, required)

__Returns__

A Base64 decoded string.

__Examples__

```lua
local decoded = coronium.utils.decode_b64( base64_str )
```

---

# utils.escape

```lua
coronium.utils.escape( string )
```

---

Encodes a URI string.

__Parameters__

Name|Details
----|-------
string|A string to escape. (String, required)

__Returns__

An encoded string.

__Examples__

```lua
local encoded_uri = coronium.utils.escape( unescaped_uri )
```

---

# utils.unescape

```lua
coronium.utils.unescape( string )
```

---

Decodes a URI string.

__Parameters__

Name|Details
----|-------
string|A string to unescape. (String, required)

__Returns__

An encoded string.

__Examples__

```lua
local decoded_uri = coronium.utils.unescape( escaped_uri )
```

---

# utils.encode_args

```lua
coronium.utils.encode_args( args_tbl )
```

---

Encodes arugments into a URI string from a table.

__Parameters__

Name|Details
----|-------
table|A table of arguments. (Table, required)

__Returns__

An encoded string.

__Examples__

```lua
local arg_str = coronium.utils.encode_args( args_tbl )
```

---

# utils.decode_args

```lua
coronium.utils.decode_args( args_str )
```

---

Parses arugments from a URI string to a table.

__Parameters__

Name|Details
----|-------
args_str|A URI string arguments. (String, required)

__Returns__

An table with the parsed string.

__Examples__

```lua
local args_tbl = coronium.utils.decode_args( args_str )
```


---


# utils.md5

```lua
coronium.utils.md5( string )
```

---

Create a MD5 hash.

__Parameters__

Name|Details
----|-------
string|A string to MD5. (String, required)

__Returns__

A MD5 hash.

__Examples__

```lua
local hashmd5 = coronium.utils.md5( str )
```

---

# globals

```lua
coronium.globals
```

---

A global space to store information that will be available to all cloud code pages.

__Examples__

```lua
-- set a global
coronium.globals.some_data = some_data
-- get a global
local some_data = coronium.globals.some_data
```

---

# asJson

```lua
coronium.asJson( table )
```

---

Converts a Lua table to a JSON string. Useful for log output.

__Parameters__

Name|Details
----|-------
table|A Lua table to convert to JSON. (Table, required)

__Returns__

A JSON string object based on the input table.

__Examples__

```lua
coronium.log( coronium.asJson( table ) )
```
