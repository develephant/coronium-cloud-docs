# Using the Module

First you need to download the latest version of the [Coronium Module for Corona SDK](https://bitbucket.org/develephant/mod_coronium/get/default.zip). Make sure to include the `mod_coronium.lua` in your Corona project directory. Create a new Lua file in your Corona project and bring in the Coronium Module using "require" like so:

```lua --module include
local coronium = require( "mod_coronium" )
```

## Standard HTTP

You then need to initialize the Coronium Module with your Coronium API key.

```lua --http sample
local coronium = require( "mod_coronium" )
coronium:init( { appId = "your.coronium.instance", apiKey = "your-api-key" } )
```

## Debugging Output

It can be very helpful to see the data coming into your app from Coronium, as well as what is being sent while developing your cloud app.

There are a couple of flags that you can set after your `coronium:init()` to control the debug output (which will appear in the console).


| Flag | Description | Usage | Default |
|------------:|--------------------------------------------------------------------|:-----------------------------------:|:-------:|
| showDebug | *Show full output including request and response information* | `coronium.showDebug=[true\|false]` | True |
| showVerbose | *Used to limit output to only the requested data structure* | `coronium.showVerbose=[true\|false]` | True |
| showAlert | *Useful for testing on the device. Will pop an alert with some info* | `coronium.showAlert=[true\|false]` | False |
| showJson | *Only output the raw response JSON data, pre-table* | `coronium.showJson=[true\|false]` | False |

To supress all the debug output, set `coronium.showDebug = false`, or remove/comment the flags in the code.

!!! note "Power Tip!"
    You dont need to add a callback to any of the methods when using debug flags. This makes it easy to prototype quickly.

**Examples**

```lua --flag examples
local coronium = require( "mod_coronium" )
coronium:init( { appId = "your.coronium.instance", apiKey = "your-api-key" } )

--show all debug output
coronium.showDebug = true

--show only data output
coronium.showDebug = true
coronium.showVerbose = false

--show only raw JSON request
coronium.showJson = true

--disable debug output
coronium.showDebug = false
```

## Secured HTTPS

When using an SSL connection, you will need to add the `https` flag as shown:

```lua --https example
local coronium = require( "mod_coronium" )
coronium:init( { appId = "your.coronium.instance",
apiKey = "your-api-key", https = true } )
```

## Testing the client

You can now use the [Coronium Client API](http://docs.coronium.io/en/latest/coronasdk/), for example:

```lua --client test
local coronium = require( "mod_coronium" )
coronium:init( { appId = "your.coronium.instance", apiKey = "your-api-key" } )
-- Register app open analytic event
coronium:appOpened()
```

!!! warning "Android Projects"
    You must add the internet permission for Android to your Corona project build.settings to use the client module.

```lua --android permissions
androidPermissions = {
  "android.permission.INTERNET",
}
```
