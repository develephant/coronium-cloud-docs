# appOpened

```lua
:appOpened( callback )
```

---

Sends the built-in "AppOpened" event to Coronium for Analytics.

__Parameters__

Name|Details
----|-------
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

```lua
local function onAppOpened( event )
  if not event.error then
    print( event.result )
  end
end

coronium:appOpened( onAppOpened )
```

---

# addEvent

```lua
:addEvent( eventType, eventTag , callback )
```

---

Sends a custom analytical event to Coronium for tracking.

__Parameters__

Name|Details
----|-------
eventType|A custom name of the event that you want to track. (String, required)
eventTag|A custom tag used for event options. (String, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

```lua
local function onAddEvent( event )
  if not event.error then
    print( event.result )
  end
end

coronium:addEvent( "MyEvent", "MyEventTag", onAddEvent )
```

---

# removeEvent

```lua
:removeEvent( eventType, eventTag, amount , callback )
```

---

Decreases custom analytical event by amount (default: 1).

__Parameters__

Name|Details
----|-------
eventType|The custom name of the event that you created. (String, required)
eventTag|A custom tag used for event options. (String, required)
amount|For numerical values. (Number, optional)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

---

# dropEvent

```lua
:dropEvent( eventType , callback )
```

---

Removes custom event and all event tags.

__Parameters__

Name|Details
----|-------
eventType|The custom name of the event that you created. (String, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

!!! danger "Heads Up!""
    __This will remove ALL records for the event, be careful!__

---

# dropEventTag

```lua
:dropEventTag( eventType, eventTag , callback )
```

---

Removes tag for a custom event.

__Parameters__

Name|Details
----|-------
eventType|The custom name of the event that you created. (String, required)
eventTag|The tag name to remove. (String, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

!!! danger "Heads Up!"
    __This will remove ALL records for the tag, be careful!__
