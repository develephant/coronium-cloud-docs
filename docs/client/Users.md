# getUser

```lua
:getUser( userId, callback )
```

---

Gets user data based on userId.

__Parameters__

Name|Details
----|-------
userId|The users objectId. (String, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

---

# registerUser

```lua
:registerUser( registrationData, callback )
```

---

Adds a new user.

__Parameters__

Name|Details
----|-------
registrationData|A table of data to be stored for the User record. __An email and password key are required for the User log in__. (Table, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

This method will return either a status code from Mongo DB, or the newly created Users `objectId` in the return event.

The default mode is to return the status code from Mongo, with `0` being success.

```lua
mc:registerUser({
  email = ...,
  password = ...,
  ...
}, function( event )
  print( event.result )
end)
```

__Examples__

```lua
--== Register user callback ==--
local function onRegisterUser( e )
  print( e.result )
end
--== Register the user ==--
mc:registerUser({
  email = "user@email.com", --REQUIRED
  password = "2468", --REQUIRED
  fav_color = "Blue",
  age = 24,
  dogs = { "danny", "fido", "suzy" }
}, onRegisterUser )
```

---

# loginUser

```lua
:loginUser( email, password, callback )
```

---

Logs in the user and starts a session.

__Parameters__

Name|Details
----|-------
email|The users stored email in the user record. (String, required)
password|The users password. (String, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

The User object. User will contain at minimum the following keys:

Name|Details
----|-------
objectId|Assigned public ID
sessionToken|The User session token
...|*User supplied table key/values*

__Examples__

```lua
mc:loginUser("me@home.com", "1234", function( event )
  print( event.result.sessionToken )
  print( event.result.objectId )
end)
```

---

# getMe

```lua
:getMe( callback )
```

---

Returns the currently logged in user.

__Parameters__

Name|Details
----|-------
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

---

# updateUser

```lua
:updateUser( updateData, callback )
```

---

Updates a user record.

__Parameters__

Name|Details
----|-------
updateUser|A table of data to be stored for the User record. (Table, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

---

# linkUserDevice

```lua
:linkUserDevice( deviceToken, addToPushBots, callback )
```

---

Add a user device to a user record.

__Parameters__

Name|Details
----|-------
deviceToken|The users device token. (String, required)
addToPushBots|Also register this device with PushBots.com. (Boolean, optional)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

---

# unlinkUserDevice

```lua
:unlinkUserDevice( deviceToken, removeFromPushBots, callback )
```

---

Removes a user device from a user record.

__Parameters__

Name|Details
----|-------
deviceToken|The users device token. (String, required)
removeFromPushBots|Also unregister this device from PushBots.com. (Boolean, optional)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

---

# logoutUser

```lua
:logoutUser( callback )
```

---

Logs out the currently logged in user, deleting the session.

__Parameters__

Name|Details
----|-------
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

# requestPassword

```lua
:requestPassword( email, callback )
```

Initiates a password reset.

__Parameters__

Name|Details
----|-------
email|The email of the user on record. (String, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__
