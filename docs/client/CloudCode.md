# run

```lua
:run( functionName, functionParameters, callback )
```
---

Calls a Coronium Cloud Code file and returns the results.

__Parameters__

Name | Details
-----|---------
functionName|The Cloud Code function name (String, required)
functionParameters|Table of parameters in name/value pairs to pass to the function. Pass an empty table for no parameters. (Table, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

The Cloud Code results, if any.

__Examples__

```lua
local function onRun( event )
  if not event.error then
    print( event.result.value )
  end
end

coronium:run( "hello", { username = "Chris" }, onRun )
```
