# createObject

```lua
:createObject( objClass, objDataTable, callback )
```

---

Creates a new Coronium data object with the table passed.

__Parameters__

Name|Details
----|-------
objClass|The name of the class that you want to create, (String, required)
objDataTable|Table of name / value pairs. (Table, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

```lua
local function onCreateObject( event )
  if not event.error then
    print( event.result.createdAt )
  end
end

local dataTable = { score = 1200, cheatMode = false }
coronium:createObject( "MyClass", dataTable, onCreateObject )
```

---

# updateObject

```lua
:updateObject( objClass, objId, objDataTable, callback )
```

---

Updates a Coronium data object with the table passed.

__Parameters__

Name|Details
----|-------
objClass|The name of the class that you want to update. (String, required)
objId|The id of the class that you want to update.  (String, required)
objDataTable|Table of name / value pairs to update. (Table, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

The updated object.

__Examples__

```lua
local function onUpdateObject( event )
  if not event.error then
    print( event.result.updatedAt )
  end
end

local dataTable = { score = 5200, cheatMode = true }
coronium:updateObject( "MyClass", "objectId", dataTable, onUpdateObject )
```

---

# deleteObject

```lua
:deleteObject( objClass, objId, callback )
```

---

Deletes a Coronium data object with the id passed.

__Parameters__

Name|Details
----|-------
objClass|The name of the class that you want to delete. (String, required)
objId|The id of the class that you want to delete. (String, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

```lua
local function onDeleteObject( event )
  if not event.error then
    print( event.result.value )
  end
end

coronium:deleteObject( "MyClass", "objectId", onDeleteObject )
```

---

# getObject

```lua
:getObject( objClass, objId, callback )
```

---

Gets a Coronium data object with the id passed.

__Parameters__

Name|Details
----|-------
objClass|The name of the class that you want to get. (String, required)
objId|The id of the class that you want to get. (String, required)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

__Examples__

```lua
local function onGetObject( event )
  if not event.error then
    print( event.result.objectId )
  end
end

coronium:getObject( "MyClass", "objectId", onGetObject )
```

---

# getObjects

```lua
:getObjects( objClass, queryTable, callback )
```

---

Gets Coronium data object(s) based on a query. By default returns up to 100 results. See examples below to return more/less.

!!! note ""
    Event response is returned as a table array in response.results.

__Parameters__

Name|Details
----|-------
objClass|The name of the class that you want to get. (String, required)
queryTable|If no query is provided then all results are returned. (Table, optional)
callback|The callback function. If not provided the global callback will be used. (Function, optional)

__Returns__

Lua table object.

__Examples:__

__Get objects by key__

```lua
local function onGetObjects( event )
  if not event.error then
    for _, object in ipairs( event.result ) do
      print( object.name .. " has " .. object.score )
    end
    print( "Count: " .. #event.result )
  end
end

local queryTable = {
  where = { fav_color = "blue" } }
}
coronium:getObjects( "MyClass", queryTable, onGetObjects )
```

!!! note ""
    __coronium:getObjects()__ supports [MongoDB query operators](http://docs.mongodb.org/manual/tutorial/query-documents/#specify-conditions-using-query-operators)

__Get objects less than or equal to key__

```lua
coronium:getObjects( "MyClass", {
 where = { score = { ["$lte"] = 2000 } }
}, onGetObjects )
```

__Get objects greater than key__

```lua
coronium:getObjects( "MyClass", {
 where = { age = { ["$gt"] = 21 } }
}, onGetObjects )
```

__Get objects that do not equal key__

```lua
coronium:getObjects( "MyClass", {
  where = { dog = { ["$ne"] = true } }
}, onGetObjects )
```

__Get up to 100 objects in class (default)__

```lua
coronium:getObjects( "MyClass", { where = {} }, onGetObjects )
```

__Get up to 500 objects in class__

```lua
coronium:getObjects( "MyClass", { where = {}, limit = 500 }, onGetObjects )
```

__Get up to 20 objects, sorted descending, in class__

```lua
coronium:getObjects( "MyClass",
{ limit = 20, sortField = "sortfield", sortOrder = "DESC" },
onGetObjects )
```

__Get up to 20 objects, sorted ascending, in class__

```lua
coronium:getObjects( "MyClass",
{ limit = 20, sortField = "sortfield", sortOrder = "ASC" },
onGetObjects )
```
