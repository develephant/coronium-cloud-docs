# uploadFile

---

```lua
:uploadFile( fileMetaTable, callback, progress_callback )
```

---

Uploads and stores a file. The resulting objectId will be a unique filename that you can load by appending it to the "files/" endpoint of your Coronium instance.

__Parameters:__

 Name | Details
-------|------------
fileMetaTable | A table with __filename__ and __baseDir__, and optional __destDir__ keys. (Table, required)
callback | The callback function. If not provided the global callback will be used. (Function, optional)
progress_callback | The progress callback function. Will periodically update with file transfer progress event. (Function, optional)

__Returns:__

The http path to the uploaded file.

## Directories

The default upload directory on your instance is __files__. You can add additional directories to upload to by creating new directories in your __files__ directory on the server via SFTP, or SSH. For example, to add an __imgs__ directory that you can upload your images to, make sure you have the following folder structure on your server instance, starting from the main directory: _starting dir_

```bash
  files
    /imgs
```

You will then need to specify the directory in your upload fileMeta table, using the __destDir__ key:

```lua
fileMetaTable =
{
  filename = "image.png",
  baseDir = system.DocumentsDirectory,
  destDir = "imgs"
}
```

## Examples

```lua
--File is done uploading.
local function onFileUpload( event )
  if not event.error then
    print( event.result.file )
  end
end

--File is uploading...
local function onFileProgress( event )
  print( event.bytesTransferred )
end

-- Upload file using default storage directory
local fileMetaTable =
{
  filename = "image.png",
  baseDir = system.DocumentsDirectory
}
coronium:uploadFile( fileMetaTable, onFileUpload, onFileProgress )

-- Upload file to a custom created storage directory
local fileMetaTable =
{
  filename = "image.png",
  baseDir = system.DocumentsDirectory,
  destDir = "imgs"
}
coronium:uploadFile( fileMetaTable, onFileUpload, onFileProgress )
```

!!! note "Filenames"
    The filename for the uploaded file will be in `result.file` key in the return event (if successful). You should store or associate this file with a User or other meta.

## File Types

You can currently upload the following file types:

* TEXT
* PNG
* JPG
* MOV
* M4V
* MP4
* ZIP
* PDF
* DOC
* XLS
