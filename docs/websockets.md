# Enabling WebSockets

!!! note ""
    __WebSockets are turned off by default on the Coronium Cloud Server.__

You can toggle the WebSocket functionality by logging into your Coronium instance via SSH and issuing one of the following commands:

__WebSockets ON__

```bash
sudo coronium-tools wson
```

__WebSockets OFF__

```bash
sudo coronium-tools wsoff
```

# Using WebSockets

To use the WebSockets examples in Coronium, setup like so:

1\. [Download the WebSockets example files here](https://bitbucket.org/develephant/coronium-websockets/get/default.zip)

2\. Replace the host address in __ws.html__ with your Coronium instance host address.

3\. Open __ws.html__ in a browser -- Google Chrome recommended. If using Chrome, open up the Developer Tools for easier debugging by going to your Chrome options and selecting _Tools > Developer Tools_. You will also need to set `console_output` to `true` in the __ws.html__.

4\. The default test is just an "echo" function. Once you load the __ws.html__, put data in the __Enter data__ field:

5\. Press [Return] and you should receive a value back printed to the screen.

# Programming WebSockets

The WebSockets bootstrap file is the __main.lua__ file located in the __lua/_websockets__ folder on your Coronium instance. There are two main event methods for WebSockets, `onData` and `onError`.

## onData

```lua
onData( data_in, output, close );
```

The __onData__ handler will be called when the WebSocket client sends data to the Coronium instance. The __onData__ handler has 3 incoming parameters, they are as follows:

Name|Details
----|-------
data_in|This is the incoming data from the WebSocket client. It will be a plain, or JSON formatted string. To convert this string to a Lua table use __json.encode__.
output|This is the method you use to send data back to the WebSocket client. Like other Coronium returns, you need to pack the data in a Coronium answer table.
close|This is a method that can be used to close the WebSocket connection. If you call this method, the WebSocket client will be disconnected and you will no longer receive, or be able to send, data to this client.

__Examples__

```lua
-- JSON to Lua table
local tbl = coronium.json.encode( data_in )
```

```lua
-- Return data to the websocket client
output( coronium.answer( { username = "Chris" } )
```

```lua
-- Close the connection
close()
```

## onError

```lua
onError( error_msg );
```

The `onError` method will be called when an error occurs involving the WebSocket client. In almost all cases, the client will be disconnected when an error occurs. Common errors are connection issues, and malformed data.

```lua
-- Log error
coronium.log( error_msg )
```

# Example

```lua
--== Incoming data
function ws.onData( data_in, output, close )
  -- Decode incoming string data from client
  local data = coronium.json.decode( data_in )

  -- Do work
  local answer = coronium.mongo:createObject( "friends", { username = data.username } )

  -- Output the data in an 'answer'
  output( answer )

  -- Close the connection, if needed
  close()
end

--== Error
function ws.onError( error_str )
  coronium.log( error_log )
end
```

# Corona SDK WebSockets

WebSocket support is commonly used in a web browser with the [JQuery Websocket](https://code.google.com/p/jquery-websocket/), but if you want to use a Corona SDK WebSocket client, see [DMC Websockets](http://docs.davidmccuskey.com/display/docs/Quick+Guide+-+dmc_websockets) It's recommended to use the dedicated Corona SDK [Coronium module](https://bitbucket.org/develephant/mod_coronium/wiki/Home) to interface with your Coronium instance if you're building a Corona SDK application.

# Resources

Please visit the [Coronium IO Group](http://forums.coronium.io) for more information on Coronium WebSockets support and tips.

# Examples

__You can find the WebSockets example files at [https://bitbucket.org/develephant/coronium-websockets](https://bitbucket.org/develephant/coronium-websockets)__
