# Getting your API key

You need an API key to communicate between your Corona app and the Coronium instance. In your Coronium Cloud Admin, click the __Settings__ link on the left navigation bar to view your API Key.

# Setting the Admin password

You need a username and password to access the Coronium Control Panel. The default username is __admin__ and the password is __coroniumserver__. It's a good idea to change the defaults. In your Coronium Cloud Admin, click the __Settings__ link on the left navigation bar to change the log in credentials.

# Email Settings

!!! danger ""
    The Coronium Server is a send only email server.  You can not receive email on it.

You should tailor the system email settings to your brand. You can adjust the following fields:

Field|Details
-----|-------
From Name|The display name on the sent email. Use a company or application name.
From Address|The reply-to address. Set this to an address that you receive mail, or aim it at a noreply@yourdomain.com
From Hostname|The base url for the reset password and account confirmation screens.
Confirmation Email|Toggle the account confirmation email. When enabled, a newly registered user must confirm the account via their email address.

!!! note ""
    If you don't have a hosted domain address for the Coronium Server you can put the server IP as a hostname substitute.

# PushBots Settings

Coronium Cloud uses the free [PushBots.com](http://pushbots.com) as a gateway for push messaging. You can integrate PushBots directly into the Coronium Administration which allows you to send messages directly to your users as well as use the customized send panel for Corona SDK apps.

!!! note ""
    Make sure to register an account at [PushBots.com](http://pushbots.com) and then enter your PushBots __app key__ and __secret key__ in the __Coronium Settings__ area.

# Regenerating the API key

If you need to regenerate your API Key, SSH into your Coronium instance and use the following command:

```bash
cd /home/coronium
sudo coronium-tools newapikey
```

!!! danger "API Key Regeneration"
    You should only regenerate the key when absolutely needed. Once you regenerate the API Key, you will not be able to recover the old key. __If you have live apps using the current API Key, they will no longer be able to connect to your Coronium Cloud instance once the key is regenerated__.
