# Common Issues

__Error 500 / Can't access admin__

Make sure the Coronium process is running. Log into your instance via SSH and issue the following command:

```bash
sudo service coronium start
```

Wait a minute or two and then try the admin page again.

__Error 502 / Bad Gateway__

Apache is still initializing. Wait a few minutes and refresh the page.

# Viewing Logs

If you can't access the admin panel to view the error log, you can check the logs manually by connecting to your instance via SSH and issuing the following command:

```bash
cat /home/coronium/logs/error.log
```

Look for any errors reported and fix them accordingly.
