When you're logged into the Coronium Cloud server through SSH, you have access to certain commands to control your instance.

__Start out in your home (coronium) directory:__

```bash
cd /home/coronium
```

# Controlling Coronium

Function|Command
--------|-------
Start Coronium|__sudo service coronium start__
Stop Coronium|__sudo service coronium stop__
Reload Coronium|__sudo service coronium reload__

# Coronium Tools

!!! danger "Heads Up!"
    Some of the following commands may have harmful consequences or specific set-up needs. Please click the links on the entries that have them before running those commands.

Function|Command
--------|-------
Update Coronium library|__sudo coronium-tools update__
[Toggle WebSockets support](Config/#using-websockets)|__sudo coronium-tools wson/wsoff__
[Toggle SSL/HTTPS support](ssl)|__sudo coronium-tools sslon/ssloff__
Install modules|__sudo coronium-tools install <module_name>__
[Generate new API Key](Config/#regenerating-the-api-key)|__sudo coronium-tools newapikey__
