Once you're ready to go into production with your Coronium app, you can gain a fairly significant speed boost by enabling the Lua Code Cache.  First, log into your Coronium instance via SSH, and then enable or disable the cache by following the directions below.

!!! warning "Heads Up!"
    With the Lua cache enabled, you must manually reload Coronium after __any__ change to a file.


To toggle the Lua Code Cache, log into your Coronium instance via SSH and issue one of the following commands:

__Lua Code Cache ON__

```bash
sudo coronium-tools cacheon
```

__Lua Code Cache OFF__

```bash
sudo coronium-tools cacheoff
```

# Reloading Coronium

To reload Coronium, issue the following on the command line:

```bash
sudo service coronium reload
```
