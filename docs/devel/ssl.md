# Set up SSL/HTTPS for Coronium

!!! warning ""
    __Make sure to point a validated domain name to your Coronium instance using your DNS provider.__

!!! note ""
    Once you have your __ssl__ files, you need to upload them to your Coronium instance via SFTP. Create a new folder in your Coronium user directory named __ssl__, and upload both your __ssl.key__ and __ssl.crt__ files into the __ssl__ directory

__Next, log into your Coronium instance via SSH and copy and paste the following block of code into the terminal to run the ssl configuration utility, press [Enter]:__

```bash
cd /home/coronium && \
sudo wget https://s3.amazonaws.com/coronium-support/ssl/ssl_config && \
sudo chmod +x ssl_config; sudo ./ssl_config
```

 To access your admin, you will need to include the __https__ in the domain. Be sure to [add the __https__ flag](../coronasdk.md) for your Corona SDK Coronium client as well.
