Place Lua files in the Lua directory to run them as server-side code. You can run the code using the client module.

# Getting Client Input

The client module will send up a table of arguments that can be consumed by the server-side code. To get that table, we use:  
```lua
coronium.input()
```

```lua
local in_data = coronium.input()
local username = in_data.username
```

# Sending Client Output

We also need to send back an "answer" data table to the client to return data values from the request. To do that, we use:
 
```lua
coronium.output( coronium.answer( out_data ) )
```

```lua
local out_data = { greeting = "Hello" }
coronium.output( coronium.answer( out_data ) )
```

!!! note ""
    All Coronium library methods return an "answer" table. You can pass these directly through the coronium.output() method.

```lua
local answer = coronium.php.run( "greeting", { username = in_data.username } )
coronium.output( answer )
```

# An Echo Example

```lua
local in_data = coronium.input()
local answer = coronium.answer( { greeting = "Hello " .. in_data.username } )
coronium.output( answer )
```

!!! warning ""
    All data in the table must be able to be encoded and decoded with JSON.

# Sending Client Errors

If you need to send an error message back based on your code, you can output an error message to the client with:

```lua
coronium.outputError( errorStr, errorCode )
```

All Coronium library methods return "answer" tables with error, and errorCodes. You can pass these directly with output(). outputError() is for custom error handling.

__You must use only one output method for each request. For example, If you use outputError(), do not use output().__

# Video Tutorial

[Video Tutorial on YouTube](http://www.youtube.com/watch?v=jH4eOJilS28)
