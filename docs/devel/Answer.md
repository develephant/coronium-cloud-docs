For uniformity, every Coronium method returns an "answer" table (unless noted otherwise) with the following keys:

## .result

The Coronium method result. This may be a single value, a table, a table array, or a table array of tables. Check with the API documentation to see what the result will hold.

## .error

If there is an error the error key will hold the string representation. If there is no error, the error key will be nil.

## .errorCode

If there is an error, there may optionally be an errorCode. If there is no errorCode it will be nil. Not all errors return errorCodes.

## Example

```lua
local answer = coronium.mysql.query( { database = "mydb" },
"SELECT * FROM users" )

if not answer.error then
  print( #answer.result )
else
  print( answer.error )
end
```

!!! note "Answer Returns"
    These "answer" tables are the default return for every Coronium library call, so results and errors can easily be passed all the way down to the client.
