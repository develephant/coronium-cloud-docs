Easily create Lua cloud modules for your Corona® SDK applications and games,
adding cloud functionality like analytics, data objects, user management, push, uploads, email, and more.

<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://coronium.io" data-text="Free Cloud Stack for your Corona SDK Apps and Games." data-via="develephant" data-size="large" data-related="coroniumio" data-hashtags="coronasdk">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

# Installation

#### Amazon EC2 Instance (recommended)

_Select a region below to get started..._

  * __[North America](https://console.aws.amazon.com/ec2/home?region=us-east-1#LaunchInstanceWizard:ami=ami-92ae90f8)__

  * __[European Union](https://eu-west-1.console.aws.amazon.com/ec2/home?region=eu-west-1#LaunchInstanceWizard:ami=ami-f5af1186)__

  * __[Asia Pacific](https://ap-southeast-2.console.aws.amazon.com/ec2/home?region=ap-southeast-2#LaunchInstanceWizard:ami=ami-e84da187)__

---

__DigitalOcean / Ubuntu 14.04 LTS Script (advanced) __

  * Create an instance with a fresh install of Ubuntu 14.04.x LTS.
  * Log into the newly created instance via SSH as `root`.
  * Copy and paste the following code to start the installation:

```bash
wget https://s3.amazonaws.com/install-coronium/install.sh; chmod +x install.sh; ./install.sh
```

_Your instance will reboot at the end of the installation._

!!! note "Support Coronium!"
    Get a __DigitalOcean__ account with the following link to grab a $10 credit, and support __Coronium__ test servers.  __[Click Here to Sign up with DigitalOcean](https://m.do.co/c/cddeeddbbdb8)__.

# Administration

You can find your Coronium Cloud admin panel at:

```html
http://your.coronium.instance/admin
```

!!! note "First Time Login?"
    The default log in is user __admin__ and password __coroniumserver__.
    You can change your log-in credentials in the [Admin panel](devel/Config/#setting-the-admin-password).

# SFTP / Files

To move files to and from your instance, you need an SFTP client set up with your login credentials:

Field|Details
-----|-------
Hostname|__your.coronium.instance__
User|__coronium__ for Ubuntu/DigitalOcean. __ubuntu__ for Amazon EC2.
Password|Amazon EC2 uses SSH Key. Ubuntu/DigitalOcean uses password entered at install.

!!! warning "Amazon EC2 Users"
    When you first log into an EC2 instance, you will be in the __ubuntu__ folder. Navigate back a directory and then choose the __coronium__ folder to add your files. You can use __cd /home/coronium__ if you're logged in with SSH.

# Directories

The following are the Coronium Cloud related directories, and the purpose for each. These directories can be found on the server instance at __/home/coronium__.

!!! danger "Heads Up!"
    Please don't remove any of these directories.

Purpose|Directory
-------|---------
Cloud Lua|__/lua__
Start Up Lua| __/lua/_boot__
Templates|__/tpl__
Uploads|__/files__
PHP|__/php__

# Support

Visit the [Coronium IO Community](http://forums.coronium.io) for support related discussions.
