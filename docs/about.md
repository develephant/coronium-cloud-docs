# About Coronium Cloud

Coronium Cloud is a free software stack aimed at providing cloud functionality to Corona SDK apps and games.

Coronium Cloud is developed, documented, and maintained by Chris Byerley.

<!--Twitter button-->
<a href="https://twitter.com/develephant" class="twitter-follow-button" data-show-count="true" data-size="large">Follow @develephant</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

All rights reserved by the original owners of any content, code, logos, trade names, trademarks, etc.

# The Stack

**Coronium Cloud** is built on the following technologies:

* Nginx/OpenResty
* Lua
* MongoDB
* MySQL
* PHP
* CoroniumLib
* Various OSS tools

# Support Coronium

Hundreds upon hundreds of hours have been spent to develop and support **Coronium Cloud**. If you would like to show your support for free software, please consider one of the following options:

**Sign up with DigitalOcean**

Coronium development requires a lot of server time. Sign up with [DigitalOcean](https://www.digitalocean.com/?refcode=cddeeddbbdb8) using the Coronium affiliate link, and help offset that with server credits.

!!! note "Bonus 2 Months Free"
    If you are a new [DigitalOcean](https://www.digitalocean.com/?refcode=cddeeddbbdb8) user, you will also get 2 free months credit to use    the services using the link.

[Click Here to Sign up with DigitalOcean](https://www.digitalocean.com/?refcode=cddeeddbbdb8)

**Make a PayPal Donation**

Coffee fuels innovation. Anything helps.

[Click Here to Donate with PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=JKKP3GMZ8SWW4)

**Tweet to your Followers**

Spread the word about **Coronium Cloud**. Stand with free software.

<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://coronium.io" data-text="Free Cloud Stack for your Corona SDK Apps and Games." data-via="develephant" data-size="large" data-related="coroniumio" data-hashtags="coronasdk">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
